
public class Adult implements Visitable {
	
	private String name;
	private int gender;

	Adult () {
		this("Nicole", 1);
	}
	Adult (String name, int gender) {
		this.name = name;
		this.gender = gender;
	}
	
	public String getName() {
		return name;
	}
	
	public String getGender() {
		if (gender == 0) {
			return "He";
		}
		else {
			return "She";
		}
	}
	
	@Override
	public String accept(Visitor v) {
		return v.visit(this);
	}

}
