
public class VisitorDemo {

	public static void main(String[] args) {
		
		Visitor visitor = new CustomerVisitor();
		
		Kid kid = new Kid("Mandy", 1);
		Student student = new Student("Joel", 0);
		Adult adult = new Adult("Grace", 1);
		Senior senior = new Senior();
		
		System.out.println(kid.accept(visitor));
		System.out.println(student.accept(visitor));
		System.out.println(adult.accept(visitor));
		System.out.println(senior.accept(visitor));

	}

}
