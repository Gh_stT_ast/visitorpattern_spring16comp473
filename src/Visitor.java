
public interface Visitor {
	
	public String visit (Kid kid);
	public String visit (Student student);
	public String visit (Adult adult);
	public String visit (Senior senior);

}
