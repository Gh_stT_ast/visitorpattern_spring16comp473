

public class CustomerVisitor implements Visitor {
	
	public CustomerVisitor() {
		
	}

	@Override
	public String visit(Kid kid) {
		return String.format("%s is young enough to order from the kids menu. \n"
				+ "%s likes chicken nuggets. \n", kid.getName(), kid.getGender());
	}

	@Override
	public String visit(Student student) {
		return String.format("%s is a broke college student. \n"
				+ "%s orders from the dollar menu. \n", student.getName(), student.getGender());
	}

	@Override
	public String visit(Adult adult) {
		return String.format("%s works full-time and needs something quick to eat. \n"
			+ "%s goes with the first combo on the menu. \n", adult.getName(), adult.getGender());
	}

	@Override
	public String visit(Senior senior) {
		return String.format("%s enjoys having breakfast early in the morning. \n"
				+ "%s enjoys coffee with friends. \n", senior.getName(), senior.getGender());
	}
}
