
public class Senior implements Visitable {
	
	private String name;
	private int gender;

	public Senior() {
		this("Alfred", 0);
	}
	public Senior (String name, int gender) {
		this.name = name;
		this.gender = gender;
	}
	
	public String getName() {
		return name;
	}
	
	public String getGender() {
		if (gender == 0) {
			return "He";
		}
		else {
			return "She";
		}
	}
	
	@Override
	public String accept(Visitor v) {
		return v.visit(this);
	}

}
